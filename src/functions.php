<?php
/*
* Author: Martin Vician <info@vician.cz>
* Licence: MIT
*/

if(!isset($_SERVER)) die("ERROR: Without Server variable!");
if(!isset($_SERVER['REMOTE_ADDR'])) die("ERROR: Without Remote ip variable!");

function get_ip($ip=NULL){
	if ($ip == NULL) {
		if (isset($_SERVER['HTTP_X_REAL_IP'])) {
			$ip = $_SERVER['HTTP_X_REAL_IP'];
		} elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
	}
	return $ip;
}

function get_6to4_ip($ip=NULL){
	$ip = get_ip($ip);

	$parts = explode(".",$ip);

	if (count($parts) != 4) {
		// Probably IPv6 address - return it
		return $ip;
	}

	$ipv6to4 = "2002";
	for ($i = 0; $i < count($parts); $i++) {
		if ($i % 2 == 0) $ipv6to4 .= ":";
		$part = dechex($parts[$i]);
		if (strlen($part) < 2) $part = "0".$part;
		$ipv6to4 .= $part;
	}
	$ipv6to4 .= "::";

	return "$ipv6to4";
}
function get_loc_json($ip=NULL,$loc=NULL) {
	if ($loc == NULL)
		$loc = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
	return $loc;
}

function get_city_region_country($ip=NULL,$loc=NULL) {
	$ip = get_ip($ip);
	$loc = get_loc_json($ip,$loc);

	if($loc->city != "")
		$city = $loc->city." ";
	if($loc->region != "")
	$city .= $loc->region." ";
	$city .= $loc->country;
	return $city;
}

function get_city_country($ip=NULL,$loc=NULL) {
	$ip = get_ip($ip);
	$loc = get_loc_json($ip,$loc);

	$country = $loc->country;
	return $country;
}
function get_flag($ip=NULL,$loc=NULL) {
	$ip = get_ip($ip);
	$loc = get_loc_json($ip,$loc);

	$country = get_city_country($ip,$loc);
	$flag = "<img src=\"/flags/blank.gif\" class=\"flag flag-".strtolower($country)."\" alt=\"".$country."\" />";
	return $flag;
}

function get_city_region_country_flag($ip=NULL,$loc=NULL) {
	$ip = get_ip($ip);
	$loc = get_loc_json($ip,$loc);

	$city = get_city_region_country($ip,$loc)." ".get_flag($ip,$loc);
	return $city;
}
?>
