## Web pages

- https://ip.vician.net - shows your public IPv4 and IPv6 addresses
- https://ip.vician.cz - the same in Czech

## Text versions

_Suitable for scripts or automation._

- https://ipv4.vician.net - shows text version of your public IPv4 address
- https://ipv6.vician.net - shows text version of your public IPv6 address

## 6to4

https://ipv4.vician.net/6to4.php - count your 6to4 prefix


## Future

- http://php.net/manual/en/book.geoip.php
